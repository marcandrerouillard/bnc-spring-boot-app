package com.edgenda.bnc.skillsmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.edgenda.bnc.skillsmanager.model.Skill;
import com.edgenda.bnc.skillsmanager.service.SkillService;

@RestController
@RequestMapping(path = "/skills")
public class SkillController {

	private final SkillService skillService;

    @Autowired
    public SkillController(SkillService skillService) {
        this.skillService = skillService;
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public Skill getSkill(@PathVariable String id) {
        return skillService.getSkill(Long.valueOf(id));
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public List<Skill> getAllSkills() {
        return skillService.getSkills();
    }
    
    
}
