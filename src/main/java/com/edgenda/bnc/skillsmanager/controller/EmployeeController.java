package com.edgenda.bnc.skillsmanager.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.edgenda.bnc.skillsmanager.model.Employee;
import com.edgenda.bnc.skillsmanager.model.Skill;
import com.edgenda.bnc.skillsmanager.service.EmployeeService;
import com.edgenda.bnc.skillsmanager.service.SkillService;

@RestController
@RequestMapping(path = "/employees")
public class EmployeeController {

	private final EmployeeService employeeService;
	private final SkillService skillService;

    @Autowired
    public EmployeeController(EmployeeService employeeService, SkillService skillService) {
        this.employeeService = employeeService;
        this.skillService = skillService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Employee> getAllEmployees() {
        return employeeService.getEmployees();
    }
    
    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public Employee getEmployee(@PathVariable String id) {
        return employeeService.getEmployee(Long.valueOf(id));
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody Employee employee) {
        Employee savedEmployee = employeeService.createEmployee(employee);
        for(Skill skill : savedEmployee.getSkills()) {
        	skill.setEmployees(Arrays.asList(savedEmployee));
        	skillService.createSkill(skill);
        }
        return savedEmployee;
    }
    
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public void deleteEmployee(@PathVariable String id) {
        employeeService.deleteEmployee(Long.valueOf(id));
    }
	
    @RequestMapping(value="/{id}/skills", method = RequestMethod.GET)
    public List<Skill> getEmployeeSkills(@PathVariable String id) {
        return employeeService.getEmployeeSkills(Long.valueOf(id));
    }
    
    @RequestMapping(value="/skill/{id}", method = RequestMethod.GET)
    public List<Employee> getEmployeeSpecificSkill(@PathVariable String id) {
        return employeeService.getEmployeeBySkill(Long.valueOf(id));
    }
}
