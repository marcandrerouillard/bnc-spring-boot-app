package com.edgenda.bnc.skillsmanager.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Employee {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @Email
    @NotEmpty
    private String email;

    @ManyToMany(mappedBy = "employees")
    @JsonIgnoreProperties({"employees"})
    private List<Skill> skills;

    public Employee() {
    }

    public Employee(Long id, String firstName, String lastName, String email, List<Skill> skills) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.skills = skills;
    }

    @PersistenceConstructor
    public Employee(String firstName, String lastName, String email, List<Skill> skills) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.skills = skills;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public List<Skill> getSkills() {
        return skills;
    }
    
    public void addSkill(Skill skill) {
    	if(this.skills == null) {
    		this.skills = new ArrayList<Skill>();
    	}
    	this.skills.add(skill);
    }

    @PreRemove
    private void removeSkillsFromEmployee() {
        for (Skill skill : skills) {
            skill.getEmployees().remove(this);
        }
    }

}
